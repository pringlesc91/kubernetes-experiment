# Kubernetes Experiment

I've spent quite a bit of time recently playing around with Kubernetes as part of deployments for a project I was working on, but I've never got my hands dirty writing Deployment / Service specs myself.

This repo is simply for me to play around with, I don't expect any of this to be usable in a production environment, but rather with the help of [Kind](https://github.com/kubernetes-sigs/kind) I'm going to learn document some random deployments for Kubernetes
