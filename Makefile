.PHONY: build_nginx deploy_nginx

NGINX_IMAGE ?= luciam91/nginx-kubernetes

build_nginx:
	@docker build -t $(NGINX_IMAGE) -f nginx/Dockerfile ./nginx

deploy_nginx: build_nginx
	kubectl apply -f nginx/